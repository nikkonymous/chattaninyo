var chat = require('express')();
var http = require('http').Server(chat);
var io = require('socket.io')(http);

chat.get('/chat', function(req, res){
	res.sendFile(__dirname + '/chat.html');
});

var home = require('express')();
chat.get('/', function(req, res){
	res.sendFile(__dirname + '/index.html');
});

io.on('connection', function(socket) {
	/*console.log('a user connected');
	socket.on('disconnect', function(){
    	console.log('user disconnected');
  	});*/
	socket.on('chat message', function(msg){
    	//console.log('message: ' + msg);
    	io.emit('chat message', msg);
  	});
});

http.listen(3000, function(){
	console.log('listening on *:3000');
});


//routing
